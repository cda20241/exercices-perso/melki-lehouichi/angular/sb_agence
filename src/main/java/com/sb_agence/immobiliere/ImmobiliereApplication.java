package com.sb_agence.immobiliere;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ImmobiliereApplication {
	public static void main(String[] args) {
		SpringApplication.run(ImmobiliereApplication.class, args);
	}

}
