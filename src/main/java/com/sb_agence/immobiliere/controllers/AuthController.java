package com.sb_agence.immobiliere.controllers;

import com.sb_agence.immobiliere.dto.SessionResponse;
import com.sb_agence.immobiliere.dto.SessionRequest;
import com.sb_agence.immobiliere.models.Utilisateur;
import com.sb_agence.immobiliere.services.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
@SessionAttributes("name")
@CrossOrigin()
public class AuthController {
    @Autowired
    private final AuthService authService;

    @PostMapping("/register")
    public ResponseEntity<String> register(@RequestBody Utilisateur request) {
        return authService.register(request);
    }

    @PostMapping("/login")
    public SessionResponse authenticate(@RequestBody SessionRequest request){
        return this.authService.authenticate(request);
    }
}
