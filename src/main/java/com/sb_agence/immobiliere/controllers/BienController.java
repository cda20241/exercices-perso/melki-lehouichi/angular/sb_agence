package com.sb_agence.immobiliere.controllers;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/bien")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:4200")
public class BienController {
}
