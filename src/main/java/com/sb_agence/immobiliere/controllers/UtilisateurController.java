package com.sb_agence.immobiliere.controllers;

import com.sb_agence.immobiliere.dto.UtilisateurDto;
import com.sb_agence.immobiliere.models.Utilisateur;
import com.sb_agence.immobiliere.services.UtilisateurService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
@CrossOrigin(origins = "http://localhost:4200")
public class UtilisateurController {
    @Autowired
    private final UtilisateurService utilisateurService;

    /** ------------------ ROUTES ------------------------------------- */
    @GetMapping("/all")
    public List<UtilisateurDto> getUtilisateurs(){return utilisateurService.getAll();}

    @GetMapping("/{id}")
    public UtilisateurDto getUtilisateur(@PathVariable Long id){
        return utilisateurService.getOne(id);
    }

    @PostMapping("/add")
    public ResponseEntity<String> ajouterUtilisateur(@RequestBody Utilisateur utilisateur){
        return utilisateurService.create(utilisateur);
    }

    @PostMapping("/update/{id}")
    public Boolean ajouterUtilisateur(@PathVariable Long id, @RequestBody Utilisateur utilisateur){
        return utilisateurService.updateUtilisateur(id,utilisateur);
    }
}
