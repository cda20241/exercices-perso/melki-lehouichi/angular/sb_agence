package com.sb_agence.immobiliere.dto;

import com.sb_agence.immobiliere.enums.TypeBien;
import com.sb_agence.immobiliere.models.Adresse;
import com.sb_agence.immobiliere.models.Bien;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Data
public class BaseBienDto {
    private Long id;
    private Integer prix;
    private Integer surface;
    private Integer nbPieces;
    private String description;
    private String image;
    private Boolean disponibilite;
    private TypeBien typeBien;
    private Adresse adresse;

    public void mapper(Bien bien){
        this.id= bien.getId();
        this.prix= bien.getPrix();
        this.surface= bien.getSurface();
        this.nbPieces= bien.getNbPieces();
        this.description= bien.getDescription();
        this.image= bien.getImage();
        this.disponibilite=getDisponibilite();
        this.typeBien=getTypeBien();
        this.adresse = bien.getAdresse();
    }
}
