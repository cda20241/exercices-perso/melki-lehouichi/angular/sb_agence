package com.sb_agence.immobiliere.dto;

import com.sb_agence.immobiliere.models.Recherche;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.Date;

@RequiredArgsConstructor
@Data
public class BaseRechercheDto {
    private Long id;
    private Integer prixMin;
    private Integer prixMax;
    private Integer surface;
    private Integer nbPieces;
    private Boolean disponible;
    private Date dateRecherche;

    public void mapper(Recherche recherche){
        this.id = recherche.getId();
        this.prixMin =getPrixMin();
        this.prixMax = recherche.getPrixMax();
        this.surface =recherche.getSurface();
        this.nbPieces = recherche.getNbPieces();
        this.disponible = recherche.getDisponible();
        this.dateRecherche = recherche.getDateRecherche();
    }
}
