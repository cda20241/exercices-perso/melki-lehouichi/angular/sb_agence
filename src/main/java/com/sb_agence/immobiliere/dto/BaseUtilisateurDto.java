package com.sb_agence.immobiliere.dto;

import com.sb_agence.immobiliere.enums.RoleUtilisateur;
import com.sb_agence.immobiliere.models.Adresse;
import com.sb_agence.immobiliere.models.Utilisateur;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Data
public class BaseUtilisateurDto {
    private Long id;
    private String nom;
    private String prenom;
    private String email;
    private String motDePasse;
    private String telephone;
    private RoleUtilisateur role;
    private Adresse adresse;


    public void mapper(Utilisateur utilisateur){
        this.id = utilisateur.getId();
        this.nom=utilisateur.getNom();
        this.prenom= utilisateur.getPrenom();
        this.email=utilisateur.getEmail();
        this.motDePasse= utilisateur.getPassword();
        this.telephone= utilisateur.getTelephone();
        this.role=utilisateur.getRole();
        this.adresse= utilisateur.getAdresse();
    }
}
