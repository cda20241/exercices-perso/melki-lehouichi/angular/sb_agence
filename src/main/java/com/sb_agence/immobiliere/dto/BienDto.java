package com.sb_agence.immobiliere.dto;

import com.sb_agence.immobiliere.models.Bien;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Data
public class BienDto extends BaseBienDto {
    private BaseUtilisateurDto proprietaire;

    public void mapper(Bien bien){
        super.mapper(bien);
        this.proprietaire=new BaseUtilisateurDto();
        this.proprietaire.mapper(bien.getProprietaire());
    }
}
