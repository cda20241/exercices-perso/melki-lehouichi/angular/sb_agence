package com.sb_agence.immobiliere.dto;

import com.sb_agence.immobiliere.models.Recherche;

public class RechercheDto extends BaseRechercheDto {
    private BaseUtilisateurDto utilisateur;

    public void mapper(Recherche recherche){
        super.mapper(recherche);
        this.utilisateur = new BaseUtilisateurDto();
        this.utilisateur.mapper(recherche.getUtilisateur());
    }
}
