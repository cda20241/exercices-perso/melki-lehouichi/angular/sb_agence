package com.sb_agence.immobiliere.dto;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Data
public class SessionRequest {
    private String email;
    private String password;
}
