package com.sb_agence.immobiliere.dto;

import com.sb_agence.immobiliere.enums.RoleUtilisateur;
import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SessionResponse {
    private Long id;
    private Boolean isLogged;
    private RoleUtilisateur role;
    private String token;
}
