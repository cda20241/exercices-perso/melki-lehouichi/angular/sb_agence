package com.sb_agence.immobiliere.dto;

import com.sb_agence.immobiliere.models.Bien;
import com.sb_agence.immobiliere.models.Recherche;
import com.sb_agence.immobiliere.models.Utilisateur;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
@Data
public class UtilisateurDto extends BaseUtilisateurDto {
    private List<BaseBienDto> biensPossedes;
    private List<BaseRechercheDto> recherches;

    public void mapper(Utilisateur utilisateur){
        super.mapper(utilisateur);
        for(Bien bien : utilisateur.getBiensPossedes()){
            BaseBienDto newBien = new BaseBienDto();
            newBien.mapper(bien);
            this.biensPossedes.add(newBien);
        }
        for (Recherche recherche : utilisateur.getRecherches()){
            BaseRechercheDto newRecherche = new BaseRechercheDto();
            newRecherche.mapper(recherche);
            this.recherches.add(newRecherche);
        }
    }
}
