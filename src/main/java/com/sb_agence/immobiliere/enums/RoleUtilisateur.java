package com.sb_agence.immobiliere.enums;

public enum RoleUtilisateur {
    ADMIN,
    PROPRIETAIRE,
    AGENT,
    CLIENT
}
