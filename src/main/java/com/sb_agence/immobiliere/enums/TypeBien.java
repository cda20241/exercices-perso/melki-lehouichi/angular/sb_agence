package com.sb_agence.immobiliere.enums;

public enum TypeBien {
    APPARTEMENT,
    IMMEUBLE,
    MAISON
}
