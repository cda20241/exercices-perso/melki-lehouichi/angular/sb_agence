package com.sb_agence.immobiliere.models;

import jakarta.persistence.*;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Entity
@Data
@RequiredArgsConstructor
@Table(name = "adresse")
public class Adresse {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;
    @Column(name = "rue")
    private String rue;
    @Column(name = "code_postal")
    private String codePostal;
    @Column(name = "ville")
    private String ville;
}
