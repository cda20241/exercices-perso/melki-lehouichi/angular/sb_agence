package com.sb_agence.immobiliere.models;

import com.sb_agence.immobiliere.enums.TypeBien;
import jakarta.persistence.*;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Entity
@Data
@RequiredArgsConstructor
@Table(name = "bien")
public class Bien {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;
    @Column(name = "prix")
    private Integer prix;
    @Column(name = "surface")
    private Integer surface;
    @Column(name = "nb_pieces")
    private Integer nbPieces;
    @Column(name = "description")
    private String description;
    @Column(name = "image")
    private String image;
    @Column(name = "disponibilite")
    private Boolean disponibilite;

    @Column(name = "type_bien")
    @Enumerated(EnumType.STRING)
    private TypeBien typeBien;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_adresse", referencedColumnName = "id")
    private Adresse adresse;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_utilisateur", referencedColumnName = "id")
    private Utilisateur proprietaire;
}
