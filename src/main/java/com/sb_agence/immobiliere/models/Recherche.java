package com.sb_agence.immobiliere.models;

import jakarta.persistence.*;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.Date;

@Entity
@Data
@RequiredArgsConstructor
@Table(name = "recherche")
public class Recherche {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;
    @Column(name = "prix_min")
    private Integer prixMin;
    @Column(name = "prix_max")
    private Integer prixMax;
    @Column(name = "surface")
    private Integer surface;
    @Column(name = "nb_pieces")
    private Integer nbPieces;
    @Column(name = "disponible")
    private Boolean disponible;
    @Column(name = "date_recherche")
    private Date dateRecherche;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_utilisateur", referencedColumnName = "id")
    private Utilisateur utilisateur;
}
