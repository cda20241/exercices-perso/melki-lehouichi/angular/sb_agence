package com.sb_agence.immobiliere.repositorys;

import com.sb_agence.immobiliere.models.Adresse;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AdresseRepository extends JpaRepository<Adresse,Long> {
}
