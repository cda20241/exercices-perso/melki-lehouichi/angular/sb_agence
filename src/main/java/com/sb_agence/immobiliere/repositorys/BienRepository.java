package com.sb_agence.immobiliere.repositorys;

import com.sb_agence.immobiliere.models.Bien;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BienRepository extends JpaRepository<Bien,Long> {
}
