package com.sb_agence.immobiliere.repositorys;

import com.sb_agence.immobiliere.models.Recherche;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RechercheRepository extends JpaRepository<Recherche,Long> {
}
