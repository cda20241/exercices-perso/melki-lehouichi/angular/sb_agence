package com.sb_agence.immobiliere.repositorys;

import com.sb_agence.immobiliere.models.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UtilisateurRepository extends JpaRepository<Utilisateur,Long> {
    Optional<Utilisateur> findUserByEmail(String email);

}
