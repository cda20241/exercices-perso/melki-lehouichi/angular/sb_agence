package com.sb_agence.immobiliere.services;

import com.sb_agence.immobiliere.dto.BaseUtilisateurDto;
import com.sb_agence.immobiliere.dto.SessionResponse;
import com.sb_agence.immobiliere.dto.SessionRequest;
import com.sb_agence.immobiliere.dto.UtilisateurDto;
import com.sb_agence.immobiliere.enums.RoleUtilisateur;
import com.sb_agence.immobiliere.models.Adresse;
import com.sb_agence.immobiliere.models.Utilisateur;
import com.sb_agence.immobiliere.repositorys.UtilisateurRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
@RequiredArgsConstructor
public class AuthService {
    private final UtilisateurRepository utilisateurRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;
    private final AuthenticationManager authenticationManager;
    public ResponseEntity<String> register(Utilisateur request) {
        var user = Utilisateur.builder()
                .nom(request.getNom())
                .prenom(request.getPrenom())
                .email(request.getEmail())
                .password(passwordEncoder.encode(request.getPassword()))
                .telephone(request.getTelephone())
                .role(request.getRole())
                .adresse(new Adresse())
                .biensPossedes(new ArrayList<>())
                .recherches(new ArrayList<>())
                .favoris(new ArrayList<>())
                .build();
        user = utilisateurRepository.save(user);
        if (user == null) {
            return new ResponseEntity<>("Une érreur est surevenue !", HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>("L\'utilisateur "+ user.getNom() + " a été ajouté avec succès !", HttpStatus.OK);
    }
//    public Boolean register(Utilisateur request) {
//        var user = Utilisateur.builder()
//                .nom(request.getNom())
//                .prenom(request.getPrenom())
//                .email(request.getEmail())
//                .password(passwordEncoder.encode(request.getPassword()))
//                .telephone(request.getTelephone())
//                .role(request.getRole())
//                .adresse(new Adresse())
//                .biensPossedes(new ArrayList<>())
//                .recherches(new ArrayList<>())
//                .favoris(new ArrayList<>())
//                .build();
//        user = utilisateurRepository.save(user);
//            UtilisateurDto responseUser = new UtilisateurDto();
//            return true;
//    }
    public SessionResponse authenticate(SessionRequest request) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        request.getEmail(),
                        request.getPassword()
                )
        );
        var user = utilisateurRepository.findUserByEmail(request.getEmail())
                .orElseThrow();
        var jwtToken = jwtService.generateToken(user);
        return SessionResponse.builder()
                .id(user.getId())
                .isLogged(true)
                .role(user.getRole())
                .token(jwtToken)
                .build();
    }
}
