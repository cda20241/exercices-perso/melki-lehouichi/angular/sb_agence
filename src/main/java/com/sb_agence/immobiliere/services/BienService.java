package com.sb_agence.immobiliere.services;

import com.sb_agence.immobiliere.models.Adresse;
import com.sb_agence.immobiliere.models.Bien;
import com.sb_agence.immobiliere.repositorys.AdresseRepository;
import com.sb_agence.immobiliere.repositorys.BienRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class BienService {
    @Autowired
    private BienRepository bienRepository;
    @Autowired
    private AdresseRepository adresseRepository;

    /** ------------ CRUD ---------------------------------------------- */
    public ResponseEntity<String> create(Bien bien) {
        bien.setId(null);
        bien= bienRepository.save(bien);
        return new ResponseEntity<>("Le bien " + bien.getTypeBien() +
                " a été ajouté !", HttpStatus.CREATED);
    }

    public Bien addAdresseToBien(Long bienId, Adresse adresse) {
        // Sauvegarde l'adresse dans la base de données
        adresseRepository.save(adresse);

        // Récupère le bien par son ID
        Bien bien = bienRepository.findById(bienId).orElse(null);

        if (bien != null) {
            // Associe l'adresse au bien
            bien.setAdresse(adresse);

            // Sauvegarde le bien dans la base de données
            bienRepository.save(bien);
        }

        return bien;
    }
}
