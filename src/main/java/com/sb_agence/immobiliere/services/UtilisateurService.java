package com.sb_agence.immobiliere.services;

import com.sb_agence.immobiliere.dto.UtilisateurDto;
import com.sb_agence.immobiliere.enums.RoleUtilisateur;
import com.sb_agence.immobiliere.models.Adresse;
import com.sb_agence.immobiliere.models.Bien;
import com.sb_agence.immobiliere.models.Utilisateur;
import com.sb_agence.immobiliere.repositorys.AdresseRepository;
import com.sb_agence.immobiliere.repositorys.BienRepository;
import com.sb_agence.immobiliere.repositorys.UtilisateurRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UtilisateurService {
    @Autowired
    private final UtilisateurRepository utilisateurRepository;
    @Autowired
    private final AdresseRepository adresseRepository;
    @Autowired
    private final BienRepository bienRepository;
    @Autowired
    private final PasswordEncoder passwordEncoder;

    /** ------------------ CRUD ------------------------------------- */
    public List<UtilisateurDto> getAll(){
        List<Utilisateur> utilisateurs = utilisateurRepository.findAll();
        List<UtilisateurDto> resultat = new ArrayList<>();
        for (Utilisateur utilisateur : utilisateurs){
            UtilisateurDto newUtilisateur = new UtilisateurDto();
            newUtilisateur.mapper(utilisateur);
            resultat.add(newUtilisateur);
        }
        return resultat;
    }

    public UtilisateurDto getOne(Long id) {
        Utilisateur utilisateur = utilisateurRepository.findById(id).orElse(null);
        if (utilisateur != null){
            UtilisateurDto utilisateurDto = new UtilisateurDto();
            utilisateurDto.mapper(utilisateur);
            return utilisateurDto;
        }else {
            return null;
        }
    }

    public ResponseEntity<String> create(Utilisateur utilisateur) {
        var user = Utilisateur.builder()
                .nom(utilisateur.getNom())
                .prenom(utilisateur.getPrenom())
                .email(utilisateur.getEmail())
                .password(passwordEncoder.encode(utilisateur.getPassword()))
                .telephone(utilisateur.getTelephone())
                .role(utilisateur.getRole())
                .adresse(new Adresse())
                .biensPossedes(new ArrayList<>())
                .recherches(new ArrayList<>())
                .favoris(new ArrayList<>())
                .build();
        user = utilisateurRepository.save(user);
        if (user != null) {
            return new ResponseEntity<>("L\'utilisateur "+ user.getNom() + " a été ajouté avec succès !", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Une érreur est surevenue !", HttpStatus.NOT_FOUND);
        }
    }

    public Boolean updateUtilisateur(Long id,Utilisateur utilisateur){
        var user = Utilisateur.builder()
                .id(id)
                .nom(utilisateur.getNom())
                .prenom(utilisateur.getPrenom())
                .email(utilisateur.getEmail())
                .password(passwordEncoder.encode(utilisateur.getPassword()))
                .telephone(utilisateur.getTelephone())
                .role(utilisateur.getRole())
                .adresse(new Adresse())
                .biensPossedes(new ArrayList<>())
                .recherches(new ArrayList<>())
                .favoris(new ArrayList<>())
                .build();
        utilisateur = utilisateurRepository.save(user);
        if (utilisateur != null) {
            return true;
        } else {
            return false;
        }
    }
    public ResponseEntity<String> deleteUtilisateur(Long id){
        Utilisateur utilisateur = utilisateurRepository.findById(id).orElse(null);
        if (utilisateur!=null){
            utilisateurRepository.deleteById(id);
            return new ResponseEntity<>("L\'utilisateur " + utilisateur.getNom() +" a été supprimé avec succès !",HttpStatus.OK);
        } else {
            return new ResponseEntity<>("L\'utilisateur n'existe pas !",HttpStatus.NOT_FOUND);
        }
    }

    public ResponseEntity<String> addBienToProprietaire(Long idUtilisateur, Long idBien){
        Utilisateur utilisateur = utilisateurRepository.findById(idUtilisateur).orElse(null);
        Bien bien = bienRepository.findById(idBien).orElse(null);
        if (utilisateur != null && bien != null){
            if (utilisateur.getBiensPossedes().contains(bien)){
                return new ResponseEntity<>("Le bien appartient déjà à ce propriétaire !",HttpStatus.NOT_FOUND);
            }
            utilisateur.getBiensPossedes().add(bien);
            utilisateurRepository.save(utilisateur);
            return new ResponseEntity<>("Le bien a été acquis avec succès !", HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>("Propriétaire ou Bien non trouvé !", HttpStatus.NOT_FOUND);
        }
    }

    public ResponseEntity<String> addAdresseToUtilisateur(Long idUtilisateur, Long idAdresse){
        Utilisateur utilisateur = utilisateurRepository.findById(idUtilisateur).orElse(null);
        Adresse adresse = adresseRepository.findById(idAdresse).orElse(null);
        if (utilisateur != null && adresse != null){
            if (utilisateur.getAdresse() == adresse){
                return new ResponseEntity<>("L\'utilisateur habite déjà à cette adresse !",HttpStatus.NOT_FOUND);
            }
            utilisateur.setAdresse(adresse);
            utilisateurRepository.save(utilisateur);
            return new ResponseEntity<>("L\'adresse a été ajoutée avec succès !", HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>("Adresse ou utilisateur non trouvé !", HttpStatus.NOT_FOUND);
        }
    }
    public ResponseEntity<String> UpdateAdresseUtilisateur(Long idUtilisateur, Adresse adresse){
        Utilisateur utilisateur = utilisateurRepository.findById(idUtilisateur).orElse(null);
        if (utilisateur != null && adresse != null){
            if (utilisateur.getAdresse() == adresse){
                return new ResponseEntity<>("L\'utilisateur habite déjà à cette adresse !",HttpStatus.NOT_FOUND);
            }
            utilisateur.setAdresse(adresse);
            utilisateurRepository.save(utilisateur);
            return new ResponseEntity<>("L\'adresse a été ajoutée avec succès !", HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>("Adresse ou utilisateur non trouvé !", HttpStatus.NOT_FOUND);
        }
    }

    public ResponseEntity<String> removeBienFromProprietaire(Long bienId, Long utilisateurId) {
        Optional<Bien> optionalBien = bienRepository.findById(bienId);
        Optional<Utilisateur> optionalUtilisateur = utilisateurRepository.findById(utilisateurId);

        if (optionalBien.isPresent() && optionalUtilisateur.isPresent()) {
            Bien bien = optionalBien.get();
            Utilisateur utilisateur = optionalUtilisateur.get();

            if (!utilisateur.getBiensPossedes().contains(bien)) {
                return new ResponseEntity<>("Le propriétaire ne possède pas ce bien !",HttpStatus.NOT_FOUND);
            }

            utilisateur.getBiensPossedes().remove(bien);
            utilisateurRepository.save(utilisateur);
            return new ResponseEntity<>("Le bien a été supprimé !",HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Bien ou propriétaire non trouvé !",HttpStatus.NOT_FOUND);
        }
    }
}
